from dataclasses import dataclass

from api_tests.landing_api import LandingApi


@dataclass
class Landing:
    api: LandingApi
