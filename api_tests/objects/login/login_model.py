from dataclasses import dataclass


@dataclass
class Login:
    email: str
    password: str

    @staticmethod
    def generate_body(email, password=None, name=None) -> dict:
        return {"email": email, "password": password, "name": name}
