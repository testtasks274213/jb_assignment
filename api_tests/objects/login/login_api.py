from urllib.parse import urljoin

from config import Config


class LoginApi:

    BASE_PATH = "api/user_management/v1/login"
    AUTH = "authenticate"
    CREATE_USER = "create_user"
    RESET_PASSWORD = "reset_password"

    def get_auth_url(self):
        return urljoin(Config.get_base_url(), f"{self.BASE_PATH}/{self.AUTH}")

    def get_create_user_url(self):
        return urljoin(Config.get_base_url(), f"{self.BASE_PATH}/{self.CREATE_USER}")

    def get_reset_password_url(self):
        return urljoin(Config.get_base_url(), f"{self.BASE_PATH}/{self.RESET_PASSWORD}")
