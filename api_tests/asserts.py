def assert_response_text(actual_response, expected_response):
    assert (
        actual_response == expected_response
    ), f"Error: response text {actual_response} doesn't match {expected_response}"
