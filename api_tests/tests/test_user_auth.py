import os

import allure
import pytest
import requests
from pytest_check import check

from api_tests.asserts import assert_response_text
from api_tests.constants import ResponseTexts
from api_tests.helper_fake import fake
from api_tests.objects.login.login_model import Login


@allure.feature("API tests: user auth")
class TestLandingPageLogin:
    @pytest.mark.positive
    @allure.title("Correct credentials")
    def test_check_auth_correct_creds(self, landing):
        request_body = Login.generate_body(
            email=os.environ["EMAIL"], password=os.environ["PASSWORD"]
        )
        response = requests.post(landing.api.login.get_auth_url(), json=request_body)
        with check:
            assert_response_text(response.json(), ResponseTexts.OK)

    @pytest.mark.negative
    @allure.title("Incorrect credentials, made up email")
    def test_check_auth_incorrect_creds(self, landing):
        request_body = Login.generate_body(email=fake.email(), password=fake.postcode())
        response = requests.post(landing.api.login.get_auth_url(), json=request_body)
        with check:
            assert_response_text(response.json(), ResponseTexts.INCORRECT)

    @pytest.mark.negative
    @allure.title("Incorrect password, existing email")
    def test_check_auth_incorrect_password(self, landing):
        request_body = Login.generate_body(
            email=os.environ["EMAIL"], password=fake.postcode()
        )
        response = requests.post(landing.api.login.get_auth_url(), json=request_body)
        with check:
            assert_response_text(response.json(), ResponseTexts.INCORRECT)

    @pytest.mark.negative
    @allure.title("Create new account")
    def test_check_create_new_user(self, landing):
        request_body = Login.generate_body(
            email=fake.email(), password=fake.postcode(), name=fake.first_name()
        )
        response = requests.post(
            landing.api.login.get_create_user_url(), json=request_body
        )
        with check:
            assert_response_text(
                response.json()["kind"], ResponseTexts.REGISTRATIONS_DISABLED
            )

    @pytest.mark.positive
    @allure.title("Create new account")
    def test_check_request_password(self, landing):
        request_body = Login.generate_body(email=fake.email())
        response = requests.post(
            landing.api.login.get_reset_password_url(), json=request_body
        )
        with check:
            assert_response_text(response.reason, ResponseTexts.OK)
