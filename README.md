# Assignment #

This is a project for an assignment.

**System requirements**
- python 3.9
- pytest

**URLs**
- BASE_URL: URL for tests to be run on.
This URL is stored locally in .env file

**Setup in PyCharm**
- open Run | Edit Configurations
- open Edit configuration templates
- open Pyton Tests / pytest and setup
- Working directory: path to project
- specify EMAIL and PASSWORD global variables (existing user needed)
- install requirements.txt
- create .env file to store your BASE_URL
- run ```pre-commit install``` to install linter
