import os

import allure
import logging

import pytest

from api_tests.helper_fake import fake
from config import Config

from pytest_check import check

from ui_tests.constants.error_messages import ErrorMessages
from ui_tests.pages.landing_page import (
    LandingPageCheckPresence,
    FooterCheckLinks,
    ClickabilityCheck,
    LinksCheck,
    LoginForm,
)


@allure.feature("Test landing page")
class TestLandingPage:
    @pytest.mark.positive
    @allure.title("Check presence of elements")
    def test_check_all_elements_present(self, browser):
        page = LandingPageCheckPresence(browser, Config.get_base_url())
        page.open()
        logging.info("PAGE IS OPEN")
        with check:
            LandingPageCheckPresence.check_footer(page)
        with check:
            LandingPageCheckPresence.check_login_form(page)
        with check:
            LandingPageCheckPresence.check_aside_content(page)
        with check:
            LandingPageCheckPresence.check_forgot_password_button(page)
        with check:
            LandingPageCheckPresence.check_register_button(page)

    @pytest.mark.positive
    @allure.title("Check footer links")
    def test_check_footer_links(self, browser):
        page = FooterCheckLinks(browser, Config.get_base_url())
        page.open()
        logging.info("PAGE IS OPEN")
        with check:
            FooterCheckLinks.check_blog_link(page)
        with check:
            FooterCheckLinks.check_forum_link(page)
        with check:
            FooterCheckLinks.check_support_link(page)
        with check:
            FooterCheckLinks.check_docs_link(page)

    @pytest.mark.positive
    @allure.title("Click log in, forget password and create an account options")
    def test_check_auth_options_clickability(self, browser):
        page = ClickabilityCheck(browser, Config.get_base_url())
        page.open()
        logging.info("PAGE IS OPEN")
        with check:
            ClickabilityCheck.check_create_account(page)
        with check:
            ClickabilityCheck.check_login(page)
        with check:
            ClickabilityCheck.check_forget_password(page)

    @pytest.mark.positive
    @allure.title("Click footer links")
    def test_check_footer_links_clickability(self, browser):
        page = LinksCheck(browser, Config.get_base_url())
        page.open()
        logging.info("PAGE IS OPEN")
        with check:
            LinksCheck.check_doc_click(page)
        with check:
            LinksCheck.check_blog_click(page)
        with check:
            LinksCheck.check_support_click(page)
        with check:
            LinksCheck.check_forum_click(page)

    @pytest.mark.positive
    @allure.title("Click a link in header")
    def test_check_header_clickability(self, browser):
        page = ClickabilityCheck(browser, Config.get_base_url())
        page.open()
        logging.info("PAGE IS OPEN")
        with check:
            ClickabilityCheck.check_header_click(page)

    @pytest.mark.positive
    @allure.title("Check login form visibility")
    def test_check_login_form_visibility(self, browser):
        page = LandingPageCheckPresence(browser, Config.get_base_url())
        page.open()
        logging.info("PAGE IS OPEN")
        with check:
            LandingPageCheckPresence.check_if_email_field_visible(page)
        with check:
            LandingPageCheckPresence.check_if_password_field_visible(page)
        with check:
            LandingPageCheckPresence.check_log_in_button(page)

    @pytest.mark.positive
    @allure.title("Fill and send login form with valid data")
    def test_login_form_valid_data(self, browser):
        page = LoginForm(browser, Config.get_base_url())
        page.open()
        logging.info("PAGE IS OPEN")
        with check:
            LoginForm.fill_email(page, email=os.environ["EMAIL"])
        with check:
            LoginForm.fill_password(page, password=os.environ["PASSWORD"])
        with check:
            LoginForm.press_login_button(page)
        with check:
            LoginForm.get_no_warning(page)

    @pytest.mark.negative
    @allure.title("Fill and send login form with invalid password")
    def test_login_form_invalid_password(self, browser):
        page = LoginForm(browser, Config.get_base_url())
        page.open()
        logging.info("PAGE IS OPEN")
        with check:
            LoginForm.fill_email(page, email=os.environ["EMAIL"])
        with check:
            LoginForm.fill_password(page, password=fake.postcode())
        with check:
            LoginForm.press_login_button(page)
        with check:
            LoginForm.check_warning_text(page, text=ErrorMessages.WRONG_CREDS)

    @pytest.mark.negative
    @allure.title("Fill and send login form with invalid email")
    def test_login_form_invalid_email(self, browser):
        page = LoginForm(browser, Config.get_base_url())
        page.open()
        logging.info("PAGE IS OPEN")
        with check:
            LoginForm.fill_email(page, email=fake.email())
        with check:
            LoginForm.fill_password(page, password=fake.postcode())
        with check:
            LoginForm.press_login_button(page)
        with check:
            LoginForm.check_warning_text(page, text=ErrorMessages.WRONG_CREDS)

    @pytest.mark.negative
    @allure.title("Try to register")
    def test_login_form_register(self, browser):
        page = LoginForm(browser, Config.get_base_url())
        page.open()
        logging.info("PAGE IS OPEN")
        with check:
            LoginForm.move_to_register_page(page)
        with check:
            LoginForm.fill_email(page, email=fake.email())
        with check:
            LoginForm.fill_password(page, password=fake.password())
        with check:
            LoginForm.press_register_button(page)
        with check:
            LoginForm.check_warning_text(page, text=ErrorMessages.NO_USERS_ACCEPTED)

    @pytest.mark.negative
    @allure.title("Try to register")
    def test_login_form_register_existing(self, browser):
        page = LoginForm(browser, Config.get_base_url())
        page.open()
        logging.info("PAGE IS OPEN")
        with check:
            LoginForm.move_to_register_page(page)
        with check:
            LoginForm.fill_email(page, email=os.environ["EMAIL"])
        with check:
            LoginForm.fill_password(page, password=fake.password())
        with check:
            LoginForm.press_register_button(page)
        with check:
            LoginForm.check_warning_text(page, text=ErrorMessages.ALREADY_REGISTERED)
