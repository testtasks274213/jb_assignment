class FooterLinks:
    SUPPORT = "mailto:contact@datalore.jetbrains.com"
    DOCUMENTATION = "https://www.jetbrains.com/help/datalore/datalore-quickstart.html"
    FORUM = "https://datalore-forum.jetbrains.com/"
    BLOG = "https://blog.jetbrains.com/datalore/"
