class ErrorMessages:
    WRONG_CREDS = "One or both of your email/password was incorrect"
    ALREADY_REGISTERED = "User with this email is already registered."
    NO_USERS_ACCEPTED = "New account registrations are disabled."
