from selenium.webdriver.common.by import By

from ui_tests.pages.base_page import BasePage
from ui_tests.constants.links import FooterLinks
from ui_tests.constants.texts import FooterTexts


class LandingPageCheckPresence(BasePage):
    def check_footer(self):
        assert self.is_element_present(
            By.CLASS_NAME, "landing__footer"
        ), "Footer is absent"

    def check_aside_content(self):
        assert self.is_element_present(
            By.CLASS_NAME, "landing__aside"
        ), "Aside content is absent"

    def check_login_form(self):
        assert self.is_element_present(
            By.CLASS_NAME, "landing__login-form"
        ), "Login form is absent"

    def check_register_button(self):
        assert self.is_element_present(
            By.XPATH, "//*[text() = 'Create an account']"
        ), "Create an account section is absent"

    def check_forgot_password_button(self):
        assert self.is_element_present(
            By.XPATH, "//*[text() = 'Forgot your password?']"
        ), "Forgot password section is absent"

    def check_if_email_field_visible(self):
        self.is_element_present(
            By.XPATH, "//input[@type='email']"
        ), "Email field is absent"

    def check_if_password_field_visible(self):
        self.is_element_present(
            By.XPATH, "//input[@type='password']"
        ), "Password field is absent"

    def check_log_in_button(self):
        assert self.is_element_present(
            By.XPATH, "//*[text() = 'Log in']"
        ), "Log in button is absent"


class FooterCheckLinks(BasePage):
    def check_support_link(self):
        assert self.is_link_ok(
            link_text=FooterTexts.SUPPORT, link=FooterLinks.SUPPORT
        ), "Support link is absent"

    def check_docs_link(self):
        assert self.is_link_ok(
            link_text=FooterTexts.DOCUMENTATION, link=FooterLinks.DOCUMENTATION
        ), "Docs link is absent"

    def check_forum_link(self):
        assert self.is_link_ok(
            link_text=FooterTexts.FORUM, link=FooterLinks.FORUM
        ), "Forum link is absent"

    def check_blog_link(self):
        assert self.is_link_ok(
            link_text=FooterTexts.BLOG, link=FooterLinks.BLOG
        ), "Blog link is absent"


class ClickabilityCheck(BasePage):
    def check_forget_password(self):
        assert self.is_element_clickable_nolink(
            xpath="//*[text() = 'Forgot your password?']"
        ), "Forgot password option is not clickable"

    def check_login(self):
        assert self.is_element_clickable_nolink(
            xpath="//*[text() = 'Log in here!']"
        ), "Login password option is not clickable"

    def check_create_account(self):
        assert self.is_element_clickable_nolink(
            xpath="//*[text() = 'Create an account']"
        ), "Create an account option is not clickable"

    def check_header_click(self):
        assert self.is_element_clickable_nolink(
            xpath="//*[text() = 'Datalore']"
        ), "Link in header is not clickable"


class LinksCheck(BasePage):
    def check_forum_click(self):
        assert self.is_element_clickable_link(
            link_text=FooterTexts.FORUM
        ), "Forum link is not clickable"

    def check_support_click(self):
        assert self.is_element_clickable_link(
            link_text=FooterTexts.SUPPORT
        ), "Support link is not clickable"

    def check_doc_click(self):
        assert self.is_element_clickable_link(
            link_text=FooterTexts.DOCUMENTATION
        ), "Doc link is not clickable"

    def check_blog_click(self):
        assert self.is_element_clickable_link(
            link_text=FooterTexts.BLOG
        ), "Blog link is not clickable"


class LoginForm(BasePage):
    def fill_email(self, email):
        assert self.find_and_fill(
            xpath="//input[@type='email']", key=email
        ), "No email field"

    def fill_password(self, password):
        assert self.find_and_fill(
            xpath="//input[@type='password']", key=password
        ), "No password field"

    def press_login_button(self):
        assert self.is_element_clickable_nolink(
            xpath="//*[text() = 'Log in']"
        ), "Login button is unclickable"

    def check_warning_text(self, text):
        self.is_element_present(
            By.XPATH, f"//*[text() = '{text}']"
        ), "Warning message is wrong"

    def get_no_warning(self):
        assert not self.is_there_warning(
            classname="alert_container"
        ), "There's login warning"

    def press_register_button(self):
        assert self.is_element_clickable_nolink(
            xpath="//*[text() = 'Create account']"
        ), "Register button is unclickable"

    def move_to_register_page(self):
        assert self.is_element_clickable_nolink(
            xpath="//*[text() = 'Create an account']"
        ), "Can't go to register page"
