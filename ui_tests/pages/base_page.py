from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By


class BasePage:
    def __init__(self, browser, url, timeout=10):
        self.browser = browser
        self.url = url
        self.browser.implicitly_wait(timeout)

    def open(self):
        self.browser.get(self.url)

    def is_element_present(self, how, what):
        try:
            self.browser.find_element(how, what)
        except NoSuchElementException:
            return False
        return True

    def is_link_ok(self, link_text, link):
        try:
            self.browser.find_element(By.LINK_TEXT, link_text).get_attribute(
                "href"
            ) == link
        except NoSuchElementException:
            return False
        return True

    def is_element_clickable_link(self, link_text):
        try:
            self.browser.find_element(By.LINK_TEXT, link_text).click()
        except NoSuchElementException:
            return False
        return True

    def is_element_clickable_nolink(self, xpath):
        try:
            self.browser.find_element(By.XPATH, xpath).click()
        except NoSuchElementException:
            return False
        return True

    def find_and_fill(self, xpath, key):
        try:
            self.browser.find_element(By.XPATH, xpath).send_keys(key)
        except NoSuchElementException:
            return False
        return True

    def is_there_warning(self, classname):
        try:
            self.browser.find_element(By.CLASS_NAME, classname)
        except NoSuchElementException:
            return False
        return True


#
