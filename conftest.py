from api_tests import landing_api
from api_tests.landing_model import Landing
import logging

import pytest
from selenium import webdriver


@pytest.fixture(scope="session")
def landing():
    api = landing_api.LandingApi()
    logging.info("LANDING FIXTURE CREATED")
    return Landing(api=api)


@pytest.fixture(scope="function")
def browser():
    logging.info("start browser for test..")
    browser = webdriver.Chrome()
    yield browser
    logging.info("quit browser..")
    browser.quit()
