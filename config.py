import json
import os
import pathlib
import typing

from dotenv import dotenv_values

start_path = pathlib.Path(__file__).parent.resolve()


class Config:
    _CONFIG_FILE: typing.Optional[str] = None
    _CONFIG = {
        **dotenv_values(f"{start_path}/.env"),
        **os.environ,
    }

    def __init__(self, config_file=None):
        assert os.path.exists(config_file)

        Config._CONFIG_FILE = config_file
        with open(config_file, "r") as file:
            Config._CONFIG = json.load(file)

    @classmethod
    def get_base_url(cls) -> str:
        return cls._CONFIG.get("BASE_URL")
